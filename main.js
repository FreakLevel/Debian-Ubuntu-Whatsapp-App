const electron = require ('electron');
const { app, BrowserWindow } = electron;

let mainWindow;

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 500
    });

    mainWindow.setTitle('Whatsapp');
    mainWindow.loadURL('https://web.whatsapp.com/');

    mainWindow.on('closed', () => {
        mainWindow = null;
    });
});